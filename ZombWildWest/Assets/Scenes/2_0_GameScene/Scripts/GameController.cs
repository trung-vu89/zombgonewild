﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

	public static bool IS_PLAYING = true;

	public GameObject path1;
	public GameObject path2;
	public GameObject path3;
	public GameObject cowboy;
	public GameObject bullet;
	public GameObject cactus;
	public GameObject zombieOakA;
	public GameObject zombieOakB;
	public GameObject bulletKit;
	public GameObject gameOverPanel;
	public Transform gameMainPanel;
	public UISlider staminaProgressBar;
	public UILabel bulletCountLabel;
	public UILabel staminaPercentage;
	public UILabel distanceLabel;
	public AudioClip gunNoBulletAudioClip;
	public AudioClip gunReloadAudioClip;
	public AudioClip screamingAudioClip;

	private const int PATH_MOVING_VELOCITY = 1;
	private const int PATH_START_POINT_Y = 1710;
	private const int PATH_DEAD_POINT_Y = -860;
	private const int COWBOY_HORIZONTAL_STEP = 2;
	private const int INIT_TIME_INTERVAL_BULLET_KIT = 100000;
	private const int INIT_TIME_INTERVAL_CACTUS = 60000;
	private const int INIT_TIME_INTERVAL_ZOMBIE = 10000;
	private Vector3 DEFAULT_LOCAL_SCALE = new Vector3(1, 1, 1);

	private UISprite mPathSprite1;
	private UISprite mPathSprite2;
	private UISprite mPathSprite3;

	private bool mIsLeftButtonClicked = false;
	private bool mIsRightButtonClicked = false;

	private int mMaxHorizontalEdge;

	private int mBulletKitInitTimeStep = 0;
	private int mCactusInitTimeStep = 0;
	private int mZombieInitTimeStep = 0;

	private int mCurrentBulletCount = 20;
	private static int mCurrentStamina = 100;
	private int mCurrentDistance = 0;

	private AudioSource mAudioSource;

	void Start () {
		mCurrentBulletCount = 20;
		mCurrentStamina = 100;
		mCurrentDistance = 0;
		CowboyController.onTargetColliderEnter += calculateStaminaOnTargetCollider;

		mAudioSource = this.gameObject.GetComponentInChildren<AudioSource>();

		mPathSprite1 = path1.gameObject.GetComponentInChildren<UISprite>();
		mPathSprite2 = path1.gameObject.GetComponentInChildren<UISprite>();
		mPathSprite3 = path1.gameObject.GetComponentInChildren<UISprite>();

		mMaxHorizontalEdge = (int) ((Screen.width / 2) * 0.8);

		bulletCountLabel.text = mCurrentBulletCount.ToString();
		staminaProgressBar.sliderValue = mCurrentStamina / 100;
		staminaPercentage.text = "100%";
	}
	
	void Update () {
		if (!IS_PLAYING) {
			return;
		}
		updatePath();

		// Update Left button press
		if (mIsLeftButtonClicked) {
			if (cowboy.transform.localPosition.x > (mMaxHorizontalEdge * -1)) {
				cowboy.transform.localPosition = new Vector3(cowboy.transform.localPosition.x - COWBOY_HORIZONTAL_STEP,
				                                             cowboy.transform.localPosition.y,
				                                             cowboy.transform.localPosition.z);
			}
		}
		// Update Right button press
		if (mIsRightButtonClicked) {
			if (cowboy.transform.localPosition.x < (mMaxHorizontalEdge * 1)) {
				cowboy.transform.localPosition = new Vector3(cowboy.transform.localPosition.x + COWBOY_HORIZONTAL_STEP,
				                                             cowboy.transform.localPosition.y,
				                                             cowboy.transform.localPosition.z);
			}
		}

		// Cactus process
		mCactusInitTimeStep += 100;
		if (mCactusInitTimeStep >= INIT_TIME_INTERVAL_CACTUS) {
			mCactusInitTimeStep = 0;
			initCactus ();
		}

		// Zombie process
		mZombieInitTimeStep += 100;
		if (mZombieInitTimeStep >= INIT_TIME_INTERVAL_ZOMBIE) {
			mCurrentDistance += 5;
			distanceLabel.text = mCurrentDistance + " m";
			mZombieInitTimeStep = 0;
			initZombie ();
		}

		mBulletKitInitTimeStep += 100;
		if (mBulletKitInitTimeStep >= INIT_TIME_INTERVAL_BULLET_KIT) {
			mBulletKitInitTimeStep = 0;
			initBulletKit ();
		}
	}

	private void updatePath() {
		if (path1.transform.localPosition.y <= PATH_DEAD_POINT_Y) {
			path1.transform.localPosition = new Vector3(
					path1.transform.localPosition.x, 
					PATH_START_POINT_Y,
					path1.transform.localPosition.z);
			mPathSprite1.spriteName = randomePathSpriteId();
		} else {
			path1.transform.localPosition = new Vector3(
					path1.transform.localPosition.x, 
					path1.transform.localPosition.y - PATH_MOVING_VELOCITY,
					path1.transform.localPosition.z);
		}

		if (path2.transform.localPosition.y <= PATH_DEAD_POINT_Y) {
			path2.transform.localPosition = new Vector3(
					path2.transform.localPosition.x, 
					PATH_START_POINT_Y,
					path2.transform.localPosition.z);
			mPathSprite2.spriteName = randomePathSpriteId();
		} else {
			path2.transform.localPosition = new Vector3(
					path2.transform.localPosition.x, 
					path2.transform.localPosition.y - PATH_MOVING_VELOCITY,
					path2.transform.localPosition.z);
		}

		if (path3.transform.localPosition.y <= PATH_DEAD_POINT_Y) {
			path3.transform.localPosition = new Vector3(
					path3.transform.localPosition.x, 
					PATH_START_POINT_Y,
					path3.transform.localPosition.z);
			mPathSprite3.spriteName = randomePathSpriteId();
		} else {
			path3.transform.localPosition = new Vector3(
					path3.transform.localPosition.x, 
					path3.transform.localPosition.y - PATH_MOVING_VELOCITY,
					path3.transform.localPosition.z);
		}
	}

	private string randomePathSpriteId() {
		int index = Random.Range(1, 4);
		return "Path_0" + index;
	}

	void onLeftButtonClick() {
		if (mIsRightButtonClicked) {
			return;
		}
		mIsLeftButtonClicked = true;
	}

	void onLeftButtonLeave() {
		mIsLeftButtonClicked = false;
	}

	void onRightButtonClick() {
		if (mIsLeftButtonClicked) {
			return;
		}
		mIsRightButtonClicked = true;
	}

	void onRightButtonLeave() {
		mIsRightButtonClicked = false;
	}

	void onBulletLeftButtonClick() {
		initBullet(TagManager.TAG_BULLET_LEFT);
	}

	void onBulletRightButtonClick() {
		initBullet(TagManager.TAG_BULLET_RIGHT);
	}

	void onBulletDefaultButtonClick() {
		initBullet(TagManager.TAG_BULLET_DEFAULT);
	}

	private void initBullet(string type) {
		if (!IS_PLAYING) {
			return;
		}
		if (mCurrentBulletCount <= 0) {
			mAudioSource.PlayOneShot (gunNoBulletAudioClip);
			return;
		}
		mCurrentBulletCount--;
		bulletCountLabel.text = mCurrentBulletCount.ToString();
		Vector3 initPosition;
		switch (type) {
			case TagManager.TAG_BULLET_LEFT:
				initPosition = new Vector3 (cowboy.transform.localPosition.x - 40, -220, -10);
			break;
			case TagManager.TAG_BULLET_RIGHT:
				initPosition = new Vector3 (cowboy.transform.localPosition.x + 40, -220, -10);
			break;
			default:
				initPosition = new Vector3 (cowboy.transform.localPosition.x, -190, -10);
			break;
		}
		
		GameObject newBullet = Instantiate (bullet, initPosition, cowboy.transform.localRotation) as GameObject;
		newBullet.tag = type;
		newBullet.SetActive (true);
		newBullet.transform.parent = gameMainPanel;
		newBullet.transform.localPosition = initPosition;
	}

	private void initCactus() {
		int randomX = Random.Range(-240, 240);
		Vector3 initPosition = new Vector3(randomX, 430, -10);
		GameObject newCactus = Instantiate (cactus, initPosition, cowboy.transform.localRotation) as GameObject;
		newCactus.tag = TagManager.TAG_CACTUS;
		newCactus.SetActive (true);
		newCactus.transform.parent = gameMainPanel;
		newCactus.transform.localPosition = initPosition;
		newCactus.transform.localScale = DEFAULT_LOCAL_SCALE;
	}

	private void initZombie() {
		int randomX = Random.Range(-240, 240);
		Vector3 initPosition = new Vector3(randomX, 430, -10);
		GameObject newZombie;
		int randomType = Random.Range(0, 2);
		if (randomType == 0) {
			newZombie = Instantiate (zombieOakA, initPosition, cowboy.transform.localRotation) as GameObject;
		} else {
			newZombie = Instantiate (zombieOakB, initPosition, cowboy.transform.localRotation) as GameObject;
		}
		newZombie.tag = TagManager.TAG_ZOMBIE;
		newZombie.transform.parent = gameMainPanel;
		newZombie.transform.localPosition = initPosition;
		newZombie.transform.localScale = DEFAULT_LOCAL_SCALE;
		newZombie.SetActive (true);
	}

	private void initBulletKit() {
		int randomX = Random.Range(-240, 240);
		Vector3 initPosition = new Vector3(randomX, 430, -10);
		GameObject newBulletKit = Instantiate (bulletKit, initPosition, cowboy.transform.localRotation) as GameObject;
		newBulletKit.tag = TagManager.TAG_BULLET_KIT;
		newBulletKit.SetActive (true);
		newBulletKit.transform.parent = gameMainPanel;
		newBulletKit.transform.localPosition = initPosition;
		newBulletKit.transform.localScale = DEFAULT_LOCAL_SCALE;
	}

	private void calculateStaminaOnTargetCollider(string targetTag) {
		switch (targetTag) {
			case TagManager.TAG_CACTUS:
				mCurrentStamina -= 10;
				break;
			case TagManager.TAG_ZOMBIE:
				mCurrentStamina -= 30;
				break;
			case TagManager.TAG_BULLET_KIT:
				mCurrentBulletCount += 10;
				bulletCountLabel.text = mCurrentBulletCount.ToString ();
				mAudioSource.PlayOneShot (gunReloadAudioClip);
				return;
		}

		float remainingStamina = ((float)mCurrentStamina / 100f);
		remainingStamina = remainingStamina > 0 ? remainingStamina : 0;
		staminaProgressBar.sliderValue = remainingStamina;
		staminaPercentage.text = (remainingStamina * 100) + "%";
		
		if (mCurrentStamina <= 0) {
			PlayerPrefs.SetInt (TagManager.PLAYER_PREFS_CURRENT_RESULT, mCurrentDistance);
			int currentBestResult = PlayerPrefs.GetInt (TagManager.PLAYER_PREFS_BEST_RESULT, 5);
			if (mCurrentDistance > currentBestResult) {
				PlayerPrefs.SetInt (TagManager.PLAYER_PREFS_BEST_RESULT, mCurrentDistance);
			}
			IS_PLAYING = false;
			mCurrentStamina = 100;
			IS_PLAYING = true;
			Application.LoadLevel ("ResultScene");
//			GameObject gameOverView = Instantiate (gameOverPanel, new Vector3(0f, 0f, 0f), new Quaternion(0f, 0f, 0f, 0f)) as GameObject;
//			gameOverView.SetActive (true);
//			gameOverView.transform.parent = gameMainPanel;
//			gameOverView.transform.localPosition = new Vector3 (0f, 0f, 0f);
//			gameOverView.transform.localScale = DEFAULT_LOCAL_SCALE;
//			StartCoroutine ("endGame");
			return;
		}

		TweenPosition tween;
		tween = cowboy.gameObject.GetComponentInChildren<TweenPosition> ();
		if (tween == null) {
			tween = cowboy.gameObject.AddComponent("TweenPosition") as TweenPosition;
		}
		tween.enabled = true;
		tween.duration = 0.5f;
		tween.from = cowboy.transform.localPosition;
		tween.to = new Vector3 (0, cowboy.transform.localPosition.y, cowboy.transform.localPosition.z);
	}

	IEnumerator endGame() {
		mAudioSource.PlayOneShot (screamingAudioClip);
		yield return new WaitForSeconds (2.5f);
		IS_PLAYING = true;
		Application.LoadLevel ("ResultScene");
	}
}

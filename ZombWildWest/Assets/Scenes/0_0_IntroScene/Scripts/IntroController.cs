﻿using UnityEngine;
using System.Collections;

public class IntroController : MonoBehaviour {

	public GameObject mBullet1;
	public GameObject mBullet2;

	public AudioClip mBulletAudioClip;
	private AudioSource mAudioSource;

	// Use this for initialization
	void Start () {
		mBullet1.gameObject.SetActive (false);
		mBullet2.gameObject.SetActive (false);
		mAudioSource = GameObject.FindObjectOfType<AudioSource> ();
	}

	IEnumerator test() {
		yield return new WaitForSeconds(0.5f);
		// Show the 1st bullet shot
		mAudioSource.PlayOneShot (mBulletAudioClip);
		mBullet1.SetActive (true);
		yield return new WaitForSeconds (0.5f);
		// Show the 2nd bullet shot
		mAudioSource.PlayOneShot (mBulletAudioClip);
		mBullet2.SetActive (true);
		yield return new WaitForSeconds (1.5f);
		Application.LoadLevel ("TopScene");
	}
}

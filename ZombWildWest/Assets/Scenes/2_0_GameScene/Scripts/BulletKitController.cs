﻿using UnityEngine;
using System.Collections;

public class BulletKitController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (!GameController.IS_PLAYING) {
			return;
		}
		this.transform.localPosition = new Vector3 (this.transform.localPosition.x, this.transform.localPosition.y - 2.5f, this.transform.localPosition.z);
	}
}

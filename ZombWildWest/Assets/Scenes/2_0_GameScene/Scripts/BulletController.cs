﻿using UnityEngine;
using System.Collections;

public class BulletController : MonoBehaviour {

	public AudioClip zombieDieAudioClip;

	private AudioSource mAudioSource;

	private string mTag;

	// Use this for initialization
	void Start () {
		mTag = this.gameObject.tag;
		mAudioSource = this.gameObject.GetComponentInChildren<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (this == null || !GameController.IS_PLAYING) {
			return;
		}
		if (this.transform.localPosition.y >= Screen.height) {
			DestroyObject (gameObject);
		}
		if (mTag.Equals(TagManager.TAG_BULLET_DEFAULT)) {
			this.transform.localPosition = new Vector3 (this.transform.localPosition.x, this.transform.localPosition.y + 2, this.transform.localPosition.z);
		} else if (mTag.Equals(TagManager.TAG_BULLET_LEFT)) {
			this.transform.localPosition = new Vector3 (this.transform.localPosition.x - 2, this.transform.localPosition.y + 2, this.transform.localPosition.z);
		} else if (mTag.Equals(TagManager.TAG_BULLET_RIGHT)) {
			this.transform.localPosition = new Vector3 (this.transform.localPosition.x + 2, this.transform.localPosition.y + 2, this.transform.localPosition.z);
		}
	}

	void OnTriggerEnter(Collider collider) {
		if (TagManager.TAG_ZOMBIE.Equals(collider.gameObject.tag)) {
			mAudioSource.PlayOneShot (zombieDieAudioClip);
			DestroyObject (collider.gameObject);
			DestroyObject (this.gameObject);
		}
	}
}

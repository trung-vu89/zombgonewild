﻿using UnityEngine;
using System.Collections;

public class ResultController : MonoBehaviour {

	public UILabel resultLabel;
	public UILabel bestResultLabel;

	void Start () {
		int currentResult = PlayerPrefs.GetInt(TagManager.PLAYER_PREFS_CURRENT_RESULT, 5);
		resultLabel.text = currentResult + " m";
		int bestResult = PlayerPrefs.GetInt (TagManager.PLAYER_PREFS_BEST_RESULT, 1000);
		bestResultLabel.text = "Best: " + bestResult + " m";
	}

	void onTopButtonClick() {
		Application.LoadLevel ("TopScene");
	}

	void onRetryButtonClick() {
		Application.LoadLevel ("GameScene");
	}
}

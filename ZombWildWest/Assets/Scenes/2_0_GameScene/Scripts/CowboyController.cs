﻿using UnityEngine;
using System.Collections;

public delegate void OnTargetColliderEnterHandler(string targetTag);

public class CowboyController : MonoBehaviour {

	public static event OnTargetColliderEnterHandler onTargetColliderEnter;

	public AudioClip bouncingSound;

	private AudioSource mAudioSource;

	// Use this for initialization
	void Start () {
		mAudioSource = this.gameObject.GetComponentInChildren<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider collider) {
		mAudioSource.PlayOneShot(bouncingSound);
		if (TagManager.TAG_CACTUS.Equals(collider.gameObject.tag)) {
			onTargetColliderEnter(TagManager.TAG_CACTUS);	
		} else if (TagManager.TAG_ZOMBIE.Equals(collider.gameObject.tag)) {
			onTargetColliderEnter (TagManager.TAG_ZOMBIE);
		} else if (TagManager.TAG_BULLET_KIT.Equals(collider.gameObject.tag)) {
			onTargetColliderEnter (TagManager.TAG_BULLET_KIT);
			DestroyObject (collider.gameObject);
		}
	}
}

﻿using UnityEngine;
using System.Collections;

public class ZombieController : MonoBehaviour {
	private const int TYPE_STRAIGHT_AHEAD = 0;
	private const int TYPE_LEFT_TO_RIGHT = 1;
	private const int TYPE_RIGHT_TO_LEFT = 2;
	private const float MOVING_HORIZONTAL_STEP = 0.2f;

	private Vector3 fromLeft = new Vector3 (-200, Screen.height, -10);
	private Vector3 fromRight = new Vector3 (180, Screen.height, -10);
	private Vector3 toLeft = new Vector3 (-Screen.height, -170, -10);
	private Vector3 toRight = new Vector3 (-Screen.height, 220, -10);

	private int mType;
	private int mMovingVelocity;
	private int mMovingHorizontalDirection;

	
	void Start () {
		mMovingVelocity = Random.Range(1, 4);
		mMovingHorizontalDirection = Random.Range(-1, 2);
	}
	
	void Update () {
		if (!GameController.IS_PLAYING) {
			return;
		}
		this.transform.localPosition = new Vector3(
				this.transform.localPosition.x + (MOVING_HORIZONTAL_STEP * mMovingHorizontalDirection), 
				this.transform.localPosition.y - mMovingVelocity, 
				this.transform.localPosition.z);
	}
}

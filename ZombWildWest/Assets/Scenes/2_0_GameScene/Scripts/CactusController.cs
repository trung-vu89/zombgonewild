﻿using UnityEngine;
using System.Collections;

public class CactusController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (this == null || !GameController.IS_PLAYING) {
			return;
		}
		if (this.transform.localPosition.y <= -Screen.height) {
			DestroyObject (gameObject);
		}
		this.transform.localPosition = new Vector3 (
				this.transform.localPosition.x, 
				this.transform.localPosition.y - 1, 
				this.transform.localPosition.z);
	}
}

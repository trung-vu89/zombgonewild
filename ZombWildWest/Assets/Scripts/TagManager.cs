﻿using UnityEngine;
using System.Collections;

public class TagManager {
	public const string TAG_CACTUS = "tag_cactus";
	public const string TAG_BULLET_DEFAULT = "bullet_tag_default";
	public const string TAG_BULLET_LEFT = "bullet_tag_left";
	public const string TAG_BULLET_RIGHT = "bullet_tag_right";
	public const string TAG_ZOMBIE = "zombie";
	public const string TAG_BULLET_KIT = "bullet_kit";

	public const string PLAYER_PREFS_CURRENT_RESULT = "current_result";
	public const string PLAYER_PREFS_BEST_RESULT = "best_result";
}
